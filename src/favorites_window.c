#include <pebble.h>
#include "favorites_window.h"
#include "view_model.h"
#include "navigation_state.h"
#include "delete_favorite_delegate.h"
#include "message_receiver.h"
#include "comm_utils.h"
#include "colors.h"

static int item_to_delete = -1;
static int just_appeared = 0;

// BEGIN AUTO-GENERATED UI CODE; DO NOT MODIFY
static Window *s_window;
static MenuLayer *s_menu_layer;

static void initialise_ui(void) {
  s_window = window_create();
  #ifndef PBL_SDK_3
    window_set_fullscreen(s_window, 0);
  #endif
  
  // s_menu_layer
  s_menu_layer = menu_layer_create(GRect(0, 0, PBL_DISPLAY_WIDTH, PBL_DISPLAY_HEIGHT));
  menu_layer_set_click_config_onto_window(s_menu_layer, s_window);
  layer_add_child(window_get_root_layer(s_window), (Layer *)s_menu_layer);
}

static void destroy_ui(void) {
  window_destroy(s_window);
  menu_layer_destroy(s_menu_layer);
}
// END AUTO-GENERATED UI CODE

static void refresh_favorites() {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "refresh_favorites 1");
  build_favorites_view_model();
  APP_LOG(APP_LOG_LEVEL_DEBUG, "refresh_favorites 2");
  menu_layer_reload_data(s_menu_layer);
  APP_LOG(APP_LOG_LEVEL_DEBUG, "refresh_favorites 3");
}

static void message_received(unsigned message) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "message_received 1");
  if (message == MESSAGE_KEY_FAVORITE_WAIT) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "message_received 2");
    refresh_favorites();
  }
}

static void tick_callback(struct tm *tick_time, TimeUnits units_changed) {
  if (just_appeared == 1) {
    just_appeared = 0;
  } else {
    send_message(MESSAGE_KEY_GET_FAVORITES);
  }
}

static int16_t menu_get_header_height_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  return MENU_CELL_BASIC_HEADER_HEIGHT;
}

static uint16_t menu_get_num_sections_callback(MenuLayer *menu_layer, void *data) {
  return get_list_size();
}

static uint16_t menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  return 1;
}

static void menu_draw_header_callback(GContext* ctx, const Layer *cell_layer, uint16_t section_index, void *data) {
  menu_cell_basic_header_draw(ctx, cell_layer, get_list_view_model()[section_index].stop_name);
}

static int16_t menu_get_cell_height_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {
  return 68;
}

static void menu_draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data) {
  char title[100], detail[100];
  
  if (get_list_size() > 0) {
    strcpy(title, get_list_view_model()[cell_index->section].lines[0].num);
    strcat(title, "    ");
    strcat(title, get_list_view_model()[cell_index->section].lines[0].waits[0].wait);
    strcpy(detail, get_list_view_model()[cell_index->section].lines[0].waits[0].terminus);
  }
  
  menu_cell_basic_draw(ctx, cell_layer, title, detail, NULL);
}

static void menu_select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data) {
  item_to_delete = cell_index->section;
  show_next(STATE_DELETE);
}

static void prepare_menu_layer() {
  // set colors
  menu_layer_set_highlight_colors(s_menu_layer, GREEN, GColorFromRGB(255, 255, 255));
}

static void handle_window_unload(Window* window) {
  destroy_ui();
}

static void handle_window_appear(Window* window) {
  if (item_to_delete != -1) {
    if (get_delete() == DELETE) {
      char message[80] = "";
      
      snprintf(message, sizeof(message), "%s;%s;%s;%i", get_list_view_model()[item_to_delete].stop_name, get_list_view_model()[item_to_delete].stop_code, get_list_view_model()[item_to_delete].lines[0].num, get_list_view_model()[item_to_delete].lines[0].waits[0].way);
      
      send_string(MESSAGE_KEY_ADD_FAVORITE, message);
      
      list_remove_item(item_to_delete);
      
      menu_layer_reload_data(s_menu_layer);
      
      if (get_list_size() == 0) {
        show_next(STATE_FAVORITES_ERROR);
        return;
      }
    }
    item_to_delete = -1;
  }
  
  set_data_updated_callback(message_received);
  
  just_appeared = 1;
  
  tick_timer_service_subscribe(MINUTE_UNIT, tick_callback);
}

static void handle_window_disappear(Window* window) {
  set_data_updated_callback(NULL);
  tick_timer_service_unsubscribe();
  send_message(MESSAGE_KEY_CANCEL_FAVORITES_FETCH);
}

void show_favorites_window(void) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "show_favorites_window 1");
  initialise_ui();
  
  menu_layer_set_callbacks(s_menu_layer, NULL, (MenuLayerCallbacks){
    .get_num_sections = menu_get_num_sections_callback,
    .get_num_rows = menu_get_num_rows_callback,
    .get_header_height = menu_get_header_height_callback,
    .draw_header = menu_draw_header_callback,
    .draw_row = menu_draw_row_callback,
    .select_click = menu_select_callback,
    .get_cell_height = menu_get_cell_height_callback,
  });
  
  prepare_menu_layer();
  
  window_set_window_handlers(s_window, (WindowHandlers) {
    .unload = handle_window_unload,
    .appear = handle_window_appear,
    .disappear = handle_window_disappear,
  });
  
  window_stack_push(s_window, true);
  
  APP_LOG(APP_LOG_LEVEL_DEBUG, "show_favorites_window 2");
  refresh_favorites();
}

void hide_favorites_window(void) {
  window_stack_remove(s_window, true);
}

