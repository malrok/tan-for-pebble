#include <pebble.h>
#include "stop_waits_window.h"
#include "view_model.h"
#include "comm_utils.h"
#include "colors.h"

static GBitmap *s_menu_icon;

// BEGIN AUTO-GENERATED UI CODE; DO NOT MODIFY
static Window *s_window;
static MenuLayer *s_menu_layer;

static void initialise_ui(void) {
  s_window = window_create();
  #ifndef PBL_SDK_3
    window_set_fullscreen(s_window, true);
  #endif
  
  // s_menu_layer
  s_menu_layer = menu_layer_create(GRect(0, 0, 144, 168));
  menu_layer_set_click_config_onto_window(s_menu_layer, s_window);
  layer_add_child(window_get_root_layer(s_window), (Layer *)s_menu_layer);
}

static void destroy_ui(void) {
  window_destroy(s_window);
  menu_layer_destroy(s_menu_layer);
}
// END AUTO-GENERATED UI CODE

static int16_t menu_get_header_height_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  return MENU_CELL_BASIC_HEADER_HEIGHT;
}

static uint16_t menu_get_num_sections_callback(MenuLayer *menu_layer, void *data) {
  return 1;
}

static uint16_t menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  return get_view_model()->lines_number;
}

static void menu_draw_header_callback(GContext* ctx, const Layer *cell_layer, uint16_t section_index, void *data) {
  menu_cell_basic_header_draw(ctx, cell_layer, get_view_model()->stop_name);
}

static void menu_draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data) {
  int row = cell_index->row;
  
  GBitmap *icon = NULL;
  if (get_view_model()->lines[row].waits[0].favorite == 1) {
    icon = s_menu_icon;
  }
  
  menu_cell_basic_draw(ctx, cell_layer, get_view_model()->lines[row].num, get_view_model()->lines[row].waits[0].terminus, icon);
}

static void menu_select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data) {
  int row = cell_index->row;
  char message[80] = "";

  snprintf(message, sizeof(message), "%s;%s;%s;%i", get_view_model()->stop_name, get_view_model()->stop_code, get_view_model()->lines[row].num, get_view_model()->lines[row].waits[0].way);

  send_string(MESSAGE_KEY_ADD_FAVORITE, message);

  if (get_view_model()->lines[row].waits[0].favorite == 0) {
    get_view_model()->lines[row].waits[0].favorite = 1;
  } else {
    get_view_model()->lines[row].waits[0].favorite = 0;
  }

  layer_mark_dirty(menu_layer_get_layer(s_menu_layer));
}

static void refresh_stops_wait(char *stop_code) {
  build_stop_view_model(stop_code);
  menu_layer_reload_data(s_menu_layer);
}

static void handle_window_unload(Window* window) {
  destroy_ui();
  gbitmap_destroy(s_menu_icon);
}

static void prepare_menu_layer() {
  // set colors
  menu_layer_set_highlight_colors(s_menu_layer, GREEN, GColorFromRGB(255, 255, 255));
}

void show_stop_waits_window(char *stop_code) {
  initialise_ui();
  
  s_menu_icon = gbitmap_create_with_resource(RESOURCE_ID_image_heart_identifier);
  
  menu_layer_set_callbacks(s_menu_layer, NULL, (MenuLayerCallbacks){
    .get_num_sections = menu_get_num_sections_callback,
    .get_num_rows = menu_get_num_rows_callback,
    .get_header_height = menu_get_header_height_callback,
    .draw_header = menu_draw_header_callback,
    .draw_row = menu_draw_row_callback,
    .select_click = menu_select_callback,
  });
  
  prepare_menu_layer();
  
  window_set_window_handlers(s_window, (WindowHandlers) {
    .unload = handle_window_unload,
  });
  window_stack_push(s_window, true);
  
  refresh_stops_wait(stop_code);
}

void hide_stop_waits_window(void) {
  window_stack_remove(s_window, true);
}

