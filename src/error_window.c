#include <pebble.h>
#include "error_window.h"

// BEGIN AUTO-GENERATED UI CODE; DO NOT MODIFY
static Window *s_window;
static GBitmap *s_res_image_error_identifier;
static GFont s_res_roboto_condensed_21;
static BitmapLayer *s_bitmaplayer_1;
static TextLayer *error_textlayer;

static void initialise_ui(void) {
  s_window = window_create();
  #ifndef PBL_SDK_3
    window_set_fullscreen(s_window, true);
  #endif
  
  s_res_image_error_identifier = gbitmap_create_with_resource(RESOURCE_ID_image_error_identifier);
  s_res_roboto_condensed_21 = fonts_get_system_font(FONT_KEY_ROBOTO_CONDENSED_21);
  // s_bitmaplayer_1
  s_bitmaplayer_1 = bitmap_layer_create(GRect(32, 10, 80, 80));
  bitmap_layer_set_bitmap(s_bitmaplayer_1, s_res_image_error_identifier);
  layer_add_child(window_get_root_layer(s_window), (Layer *)s_bitmaplayer_1);
  
  // error_textlayer
  error_textlayer = text_layer_create(GRect(2, 90, 140, 76));
  text_layer_set_text(error_textlayer, "Layer de texte");
  text_layer_set_text_alignment(error_textlayer, GTextAlignmentCenter);
  text_layer_set_font(error_textlayer, s_res_roboto_condensed_21);
  layer_add_child(window_get_root_layer(s_window), (Layer *)error_textlayer);
}

static void destroy_ui(void) {
  window_destroy(s_window);
  bitmap_layer_destroy(s_bitmaplayer_1);
  text_layer_destroy(error_textlayer);
  gbitmap_destroy(s_res_image_error_identifier);
}
// END AUTO-GENERATED UI CODE

static void handle_window_unload(Window* window) {
  destroy_ui();
}

void show_error_window(char *error) {
  initialise_ui();
  
  text_layer_set_text(error_textlayer, error);
  
  bitmap_layer_set_compositing_mode(s_bitmaplayer_1, GCompOpSet);
  
  window_set_window_handlers(s_window, (WindowHandlers) {
    .unload = handle_window_unload,
  });
  window_stack_push(s_window, true);
}

void hide_error_window(void) {
  window_stack_remove(s_window, true);
}

