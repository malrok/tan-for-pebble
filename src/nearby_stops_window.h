#include <pebble.h>

#define DOWN 0
#define UP 1
#define NONE -1

void show_nearby_stops_window(void);
void hide_nearby_stops_window(void);
