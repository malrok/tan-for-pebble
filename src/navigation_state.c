#include <pebble.h>
#include "navigation_state.h"
#include "error_window.h"
#include "loading_window.h"
#include "nearby_stops_window.h"
#include "home_window.h"
#include "stop_waits_window.h"
#include "favorites_window.h"
#include "delete_favorite_window.h"
#include "message_receiver.h"
#include "comm_utils.h"
#include "tan_data.h"

int current_state = -1;

void on_error_received(unsigned message) {
  if (message == MESSAGE_KEY_NETWORK_ERROR) {
    show_next(STATE_NETWORK_ERROR);
  }
}

void init_navigation_state() {
  set_error_received_callback(on_error_received);
}

void navigation_message_received(unsigned message) {
  if (current_state == STATE_LOADING_FAVORITES) {
    if (message == MESSAGE_KEY_FAVORITES_NUM) {
      hide_loading_window();
      show_error_window("Aucun favoris défini");
      current_state = STATE_FAVORITES_ERROR;
    } else if (message == MESSAGE_KEY_FAVORITE_WAIT) {
      hide_loading_window();
      show_favorites_window();
      current_state = STATE_FAVORITES; 
    }
  } else if (current_state == STATE_LOADING_NEARBY) {
    if (message == MESSAGE_KEY_LOCATION_STATUS) {
      if (get_location_status() == LOCATION_FOUND) {
        send_message(MESSAGE_KEY_GET_NEARBY);
      } else {
        hide_loading_window();
        show_error_window("Impossible d\'obtenir une localisation");
        current_state = STATE_NEARBY_ERROR; 
      }
    } else if (message == MESSAGE_KEY_WAIT_FOR_STOP) {
      hide_loading_window();
      show_nearby_stops_window();
      current_state = STATE_NEARBY;
    }
  }
}

void set_current_state(int state) {
  current_state = state;
}

int get_current_state() {
  return current_state;
}

void show_next(int next) {
  show_next_with_message(next, "");
}

void show_next_with_message(int next, char *mess) {
  switch (current_state) {
    case STATE_HOME:
    set_data_updated_callback(navigation_message_received);
    if (next == STATE_FAVORITES) {
      if (has_data_type(FAVORITES_TYPE) == 1) {
        set_data_type(FAVORITES_TYPE);
        show_favorites_window();
        current_state = STATE_FAVORITES_BUFFERED;
      } else {
        show_loading_window();
        current_state = STATE_LOADING_FAVORITES;
      }
      send_message(MESSAGE_KEY_GET_FAVORITES);
    } else {
      if (get_location_status() == LOCATION_FOUND) {
        if (has_data_type(NEARBY_TYPE) == 1) {
          set_data_type(NEARBY_TYPE);
          current_state = STATE_NEARBY_BUFFERED;
          show_nearby_stops_window();
        } else {
          show_loading_window();
          current_state = STATE_LOADING_NEARBY;
        }
        send_message(MESSAGE_KEY_GET_NEARBY);
      } else {
        show_loading_window();
        current_state = STATE_LOADING_NEARBY;
        send_message(MESSAGE_KEY_GET_LOCATION);
      }
    }
    break;
    
    case STATE_FAVORITES_BUFFERED:
    
    case STATE_FAVORITES:
    if (next == STATE_DELETE) {
      show_delete_favorite_window();
      current_state = next;
    } else if (next == STATE_FAVORITES_ERROR) {
      hide_favorites_window();
      show_error_window("Aucun favoris défini");
      current_state = STATE_FAVORITES_ERROR;
    } else if (next == STATE_NETWORK_ERROR) {
      hide_favorites_window();
      show_error_window("Erreur de communication");
      current_state = STATE_NETWORK_ERROR;
    }
    break;
    
    case STATE_DELETE:
    if (next == STATE_FAVORITES) {
      current_state = next;
      hide_delete_favorite_window();
    }
    break;
    
    case STATE_NEARBY_BUFFERED:
    
    case STATE_NEARBY:
    if (next == STATE_STOP) {
      show_stop_waits_window(mess);
      current_state = next;
    } else if (next == STATE_NETWORK_ERROR) {
      hide_nearby_stops_window();
      show_error_window("Erreur de communication");
      current_state = STATE_NETWORK_ERROR;
    }
    break;
    
    case STATE_STOP:
    // nothing to do
    break;
    
    case STATE_LOADING_FAVORITES:
    break;
    
    case STATE_LOADING_NEARBY:
    break;
    
    case STATE_NEARBY_ERROR:
    break;
    
    case STATE_FAVORITES_ERROR:
    break;
    
    default:
    show_home_window();
    current_state = next;
    break;
  }
}

void destroy_home() {
  hide_home_window();
}
