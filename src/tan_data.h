#pragma once
#include "cJSON.h"

#define VOID_TYPE 0
#define FAVORITES_TYPE 1
#define NEARBY_TYPE 2

/////////////////////////////////////
void init_tan_data(int);

void add_tan_data(char *);

void extend_tan_data(char *);

void push_tan_data();

int get_tan_data_size();

cJSON *get_tan_data();

int has_data_type(int);

void set_data_type(int);

void clear_data_type(int);
