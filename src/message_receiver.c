#include <pebble.h>
#include "message_receiver.h"
#include "tan_data.h"
#include "comm_utils.h"

static void (*data_updated_callback)(unsigned);
static void (*error_received_callback)(unsigned);

int nearby_stops = 0, waits_received = 0, location_status, favorites, favorites_received, refreshing_nearby = 0;

/**
 * looks for LOCATION_STATUS message in the dictionary, and parse it if found
 */
static void read_location(DictionaryIterator *iterator) {
  Tuple *data = dict_find(iterator, MESSAGE_KEY_LOCATION_STATUS);
  if (data) {
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "message LOCATION_STATUS received");
    location_status = (int)data->value->data[0];
    
    if (data_updated_callback) {
      (*data_updated_callback)(MESSAGE_KEY_LOCATION_STATUS);
    }
  }
}

/**
 * looks for FOUND_NEARBY message in the dictionary, and parse it if found
 */
static void read_found_nearby(DictionaryIterator *iterator) {
  Tuple *data = dict_find(iterator, MESSAGE_KEY_FOUND_NEARBY);
  if (data) {
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "message FOUND_NEARBY received");
    refreshing_nearby = 1;
    
    nearby_stops = data->value->int32;
    
    init_tan_data(NEARBY_TYPE);
  }
}

/**
 * processes the NEARBY_STOP message
 */
static void read_nearby_stop(DictionaryIterator *iterator) {
  Tuple *data = dict_find(iterator, MESSAGE_KEY_NEARBY_STOP);
  if (data) {
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "message NEARBY_STOP received");
    add_tan_data(data->value->cstring);
    
    if (get_tan_data_size() == nearby_stops) {
      if (data_updated_callback) {
        (*data_updated_callback)(MESSAGE_KEY_NEARBY_PROCESSED);
      }
      waits_received = 0;
      send_message(MESSAGE_KEY_GET_STOPS_WAIT);
    }
  }
}

/**
 * looks for WAIT_FOR_STOP message in the dictionary, and parse it if found
 */
static void read_wait_for_stop(DictionaryIterator *iterator) {
  Tuple *data = dict_find(iterator, MESSAGE_KEY_WAIT_FOR_STOP);
  if (data) {
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "message WAIT_FOR_STOP received");
    
    extend_tan_data(data->value->cstring);
    
    waits_received++;
    
    if (nearby_stops == waits_received && data_updated_callback) {
      refreshing_nearby = 0;
      push_tan_data();
      (*data_updated_callback)(MESSAGE_KEY_WAIT_FOR_STOP);
    }
  }
}

/**
 * looks for FAVORITES_NUM message in the dictionary, and parse it if found
 */
static void read_favorites_num(DictionaryIterator *iterator) {
  Tuple *data = dict_find(iterator, MESSAGE_KEY_FAVORITES_NUM);
  if (data) {
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "message FAVORITES_NUM received -- %i", (int)data->value->int32);
    favorites = data->value->int32;
    
    if (favorites == 0) {
      if (data_updated_callback) {
        (*data_updated_callback)(MESSAGE_KEY_FAVORITES_NUM);
      }
    } else {
      init_tan_data(FAVORITES_TYPE);
    }
  }
}

/**
 * looks for FAVORITE_HEAD message in the dictionary, and parse it if found
 */
static void read_favorite_head(DictionaryIterator *iterator) {
  Tuple *data = dict_find(iterator, MESSAGE_KEY_FAVORITE_HEAD);
  if (data) {
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "message FAVORITE_HEAD received");
    add_tan_data(data->value->cstring);
    
    favorites_received++;
    
    if (favorites == favorites_received) {
      favorites_received = 0;
      send_message(MESSAGE_KEY_GET_FAVORITES_WAIT);
    }
  }
}

/**
 * looks for FAVORITE_WAIT message in the dictionary, and parse it if found
 */
static void read_favorite_wait(DictionaryIterator *iterator) {
  Tuple *data = dict_find(iterator, MESSAGE_KEY_FAVORITE_WAIT);
  if (data) {
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "message FAVORITE_WAIT received");
    
    extend_tan_data(data->value->cstring);
    
    favorites_received++;
    
    if (favorites == favorites_received) {
      push_tan_data();
      
      favorites_received = 0;
      if (data_updated_callback) {
        (*data_updated_callback)(MESSAGE_KEY_FAVORITE_WAIT);
      }
    }
  }
}

/**
 * looks for NETWORK_ERROR message in the dictionary, and parse it if found
 */
static void read_network_error(DictionaryIterator *iterator) {
  Tuple *data = dict_find(iterator, MESSAGE_KEY_NETWORK_ERROR);
  if (data) {
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "message NETWORK_ERROR received");
    
    if (error_received_callback) {
      (*error_received_callback)(MESSAGE_KEY_NETWORK_ERROR);
    }
  }
}

static void on_message_received(DictionaryIterator *iterator) {
  read_location(iterator);
  read_found_nearby(iterator);
  read_nearby_stop(iterator);
  read_wait_for_stop(iterator);
  read_favorites_num(iterator);
  read_favorite_head(iterator);
  read_favorite_wait(iterator);
  read_network_error(iterator);
}

void set_data_updated_callback(void (*f)(unsigned)) {
  data_updated_callback = f;
}

void set_error_received_callback(void (*f)(unsigned)) {
  error_received_callback = f;
}

void message_receiver_init() {
  // sets the received message callback method
  set_received_callback(on_message_received);
}

int get_location_status() {
  return location_status;
}

int is_refreshing_nearby() {
  return refreshing_nearby;
}
