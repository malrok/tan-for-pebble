// base url, to be changed when on production
var BASE_URL = "http://open.tan.fr/ewp/";
// get all stops in nearby location
var STOP_W_GEO = "arrets.json/latitude/longitude";
// get wait time for a given stop
var WAIT_TIME = "tempsattente.json/stop";

// Position computation status
var LOCATION_PENDING = 0;
var LOCATION_FOUND = 1;
var LOCATION_ERROR = 2;
var waitingForLocation = false;
// nearby query status
var NEARBY_COMPUTING = 0;
var NEARBY_FOUND = 1;
var NEARBY_ERROR = 2;
// Choose options about the data returned
var positionOptions = {
  enableHighAccuracy: true,
  maximumAge: 0,
  timeout: 15000
};
// messages queue array
var messagesQueue = [];
var sendingMessage = false;
// position computing variables
var latitude, longitude;
var locationStatus = LOCATION_PENDING;
var nearbyStatus = NEARBY_COMPUTING;
var nearbyStops;
var previousNearbyMessageProcessed = true;
// replacement strings for JSON markups to reduce the size of the stream sent to the Pebble
var replacements = {
  '"codeLieu"':      '"c"',
  '"libelle"':       '"l"',
  '"distance"':      '"d"',
  '"ligne"':         '"g"',
  '"numLigne"':      '"n"',
  '"sens"':          '"s"',
  '"terminus"':      '"t"',
  '"infotrafic"':    '"i"',
  '"temps"':         '"p"',
  '"typeLigne"':     '"y"',
  '"arret"':         '"a"',
  '"codeArret"':     '"r"',
  '"favoris"':       '"f"',
  '"ligneFavorite"': '"e"',
  '"wait"':          '"w"'
  // LIGNESIZE z
};
var favorites = [], // [{"libelle":"libelle","codeLieu":"codeLieu","ligne":"ligne","sens":sens}]
    favoriteLines = [];
var cancelNearby = false,
    cancelFavorites = false;

init();

//////////////////////////////////////////////////////////////////////////
/**
 * init function
 */
function init() {
  // Request current position
  refreshPosition();
  
  // read favorites
  readFavorites();
}

function refreshPosition() {
  navigator.geolocation.getCurrentPosition(success, error, positionOptions);
}

function readFavorites() {
  var index = 0;
  
  favorites = JSON.parse(localStorage.getItem('favorites')) || [];
  
  for (;index < favorites.length; index++) {
    if (favoriteLines.indexOf(favorites[index].ligne) === -1) {
      favoriteLines.push(favorites[index].ligne);
    }
  }
}

function toggleFavorite(message) {
  var params = message.split(';');
  var index = 0, found = -1;
  
  if (params.length < 3 || !params[0] || !params[1] || !params[2] || !params[3]) {
    return;
  }
  
  for (;index < favorites.length; index++) {
    if (favorites[index].codeLieu === params[1] && favorites[index].ligne === params[2] && favorites[index].sens === params[3]) {
      found = index;
      break;
    }
  }
  
  if (found === -1) {
    favorites.push({"libelle":params[0],"codeLieu":params[1],"ligne":params[2],"sens":params[3]});
  } else {
    favorites.splice(index, 1);
  }
  
  localStorage.setItem('favorites', JSON.stringify(favorites));
}

function dequeueMessages() {
  var message = messagesQueue[0];
  sendingMessage = true;
  // Send the object
  Pebble.sendAppMessage(message, function() {
    messagesQueue.splice(0, 1);
    if (messagesQueue.length > 0) {
      dequeueMessages();
    } else {
      sendingMessage = false;
    }
  }, function(e) {
    setTimeout(function() {
      dequeueMessages();
    }, 100);
  });
}

/**
 * send message given in params
 */
function sendMessage(message) {
  messagesQueue.push(message);
  if (messagesQueue.length === 1 && !sendingMessage) {
    dequeueMessages();
  }
}

/**
 * succes method for geolocation
 */
function success(pos) {
  latitude  = pos.coords.latitude.toString().replace('.', ',');
  longitude = pos.coords.longitude.toString().replace('.', ',');

  locationStatus = LOCATION_FOUND;
  
  if (waitingForLocation) {
    sendMessage({LOCATION_STATUS: locationStatus});
  }
}

/**
 * error method for geolocation
 */
function error(err) {
  locationStatus = LOCATION_ERROR;
  
  if (waitingForLocation) {
    sendMessage({LOCATION_STATUS: locationStatus});
  }
}

/**
 * removes bad chars from the stream, and replace the markups by smaller strings to reduce the size of the stream
 */
function fixAndReduceJson(stream) {
  var result = stream.replace(/�/g, 'é');
  
  for (var replacement in replacements) {
    result = result.replace(new RegExp(replacement, 'g'), replacements[replacement]);
  }
  
  return result;
}

function sortByLine(a, b) {
  if (a.ligne.numLigne > b.ligne.numLigne) {
    return 1;
  } else if (a.ligne.numLigne > b.ligne.numLigne) {
    return -1;
  }
  return 0;
}

function sortByFavorite(a, b) {
  if (a.favoris && !b.favoris) {
    return -1;
  } else if (!a.favoris && b.favoris){ 
    return 1;
  } else {
    return 0;
  }
}

function getNearestStops(data) {
  var result = [], found = [];
  var size = data.length;

  for (var index = 0; index < size; index++) {
    var id = data[index].ligne.numLigne + '--' + data[index].sens;

    if (found.indexOf(id) === -1) {
      found.push(id);
      result.push(data[index]);
    }
  }
  
  return result;
}

function isFavorite(codeLieu, data) {
  var result = false;
  
  for (var index = 0; index < favorites.length; index++) {
    result = result || (favorites[index].codeLieu === codeLieu &&
      favorites[index].ligne === data.ligne.numLigne &&
      favorites[index].sens === data.sens.toString());
    if (result) break;
  }
  
  return result;
}

function extractDataForSending(codeLieu, data) {
  var result = [];
  var size = data.length;
  var current;
  var previousLine = '';
  
  for (var index = 0; index < size; index++) {
    if (!index || data[index].ligne.numLigne !== previousLine) {
      if (index) {
        // clone and push
        result.push(JSON.parse(JSON.stringify(current)));
      }
      current = {};
      current.numLigne = data[index].ligne.numLigne;
      current.wait = [];
      current.ligneFavorite = favoriteLines.indexOf(data[index].ligne.numLigne) !== -1;
    }
    
    current.wait.push({
      "sens": data[index].sens,
      "terminus": data[index].terminus,
      "temps": data[index].temps,
      "favoris": isFavorite(codeLieu, data[index])
    });
    
    previousLine = data[index].ligne.numLigne;
  }
  
  if (size) {
    // clone and push
    result.push(JSON.parse(JSON.stringify(current)));
  }
  
  return result;
}

function getWaitTimeForFavorite(favorite) {
  var url = BASE_URL + WAIT_TIME.replace("stop", favorite.codeLieu);
  var data, message, index = 0, found = false, arr = [];
  
  if (cancelFavorites) return;
  
  httpQuery(url, function(result) {
    data = JSON.parse(result.responseText);
    if (data.length > 0) {
      // we only take the desired line
      while (!found && index < data.length) {
        found = data[index].ligne.numLigne == favorite.ligne && data[index].sens.toString() == favorite.sens;
        if (!found) {
          index++;
        }
      }
      
      if (found) {
        arr.push(data[index]);
        data = extractDataForSending(favorite.codeLieu, arr);
      }
    } else {
       data.push({
         "numLigne": favorite.ligne,
         "wait": [{
           "sens": favorite.sens,
           "terminus": "pas de ligne",
           "temps": "N/A",
           "favoris": true
         }],
         "ligneFavorite": true
       });
    }
    
    // we add the codeLieu value in an object encapsulating the data
    message = '{"c":"' + favorite.codeLieu + '","g":' + fixAndReduceJson(JSON.stringify(data)) + '}';
    
    // we send the message back to Pebble
    sendMessage({FAVORITE_WAIT: message});
  }, function(result) {
    console.log('error when sending request -- status: ' + result.status);
  });
}

function sendFavorites() {
  if (favorites && !cancelFavorites) {
    // [{"libelle":"libelle","codeLieu":"codeLieu","ligne":"ligne","sens":sens}]
    for (var index = 0; index < favorites.length; index++) {
      var message = {
        "libelle": favorites[index].libelle,
        "codeLieu": favorites[index].codeLieu,
        "distance": "N/A"
      };
      sendMessage({FAVORITE_HEAD: fixAndReduceJson(JSON.stringify(message))});
    }
  }
}

function sendWaitTimeForFavorites() {
  var index;
  if (favorites && !cancelFavorites) {
    for (index = 0; index < favorites.length; index++) {
      getWaitTimeForFavorite(favorites[index]);
    }
  }
}

function getWaitTimeForStop(stop) {
  var url = BASE_URL + WAIT_TIME.replace("stop", stop.codeLieu);
  var data, message;
  
  if (cancelNearby) return;
  
  httpQuery(url, function(result) {
    data = JSON.parse(result.responseText);
    if (data.length > 0) {
      // we only take the first occurrencies of the lines as we only want the nearest times
      data = getNearestStops(data);
      
      // sort the records by line number
      data.sort(sortByLine);
      
      // sets the data in a format that can be processed in Pebble
      data = extractDataForSending(stop.codeLieu, data);
      
      // sort by favorites
      data.sort(sortByFavorite);
    } else {
      for (var index = 0; index < stop.ligne.length; index++) {
        // default data indicating no lines are available
        message = {
          "numLigne": stop.ligne[index].numLigne,
          "wait": [{
            "sens": 0,
            "terminus": "pas de ligne",
            "temps": "N/A",
            "favoris": false
          }],
          "ligneFavorite": false
        };
        data.push(message);
      }
    }
    
    // we add the codeLieu value in an object encapsulating the data
    message = '{"c":"' + stop.codeLieu + '","g":' + fixAndReduceJson(JSON.stringify(data)) + '}';
    
    // we send the message back to Pebble
    sendMessage({WAIT_FOR_STOP: message});
  }, function(result) {
    console.log('error when sending request -- status: ' + result.status);
  });
}

/**
 * queries the api to get wait time for given stops
 */
function getWaitTimeForNearbyStops() {
  if (nearbyStops && !cancelNearby) {
    var stops = JSON.parse(nearbyStops);
    
    for (var index = 0; index < stops.length; index++) {
      getWaitTimeForStop(stops[index]);
    }
  }
}

/**
 * send the nearby stop individually
 */
function sendNearbyStops(nearby) {
  var index = 0, length = nearby.length;
  
  if (cancelNearby) return;
  
  previousNearbyMessageProcessed = false;
  
  // send a message with the number of stops to receive
  sendMessage({FOUND_NEARBY: nearby.length});    
  
  // send a message per stop
  for (;index < length; index++) {
    nearby[index].ligne = undefined;
    sendMessage({NEARBY_STOP: fixAndReduceJson(JSON.stringify(nearby[index]))});
  }
}

/**
 * queries the api to get nearby stops
 */
function getNearbyStops(success, error) {
  var url = BASE_URL + STOP_W_GEO.replace("latitude", latitude).replace("longitude", longitude);
  
  if (cancelNearby) return;
  
  httpQuery(url, function(result) {
    nearbyStops = result.responseText;
    
    nearbyStatus = NEARBY_FOUND;

    var nearby = JSON.parse(nearbyStops);
    
    sendNearbyStops(nearby);

    if (success) {
      success.call(this, nearbyStatus);
    }
  }, function(result) {
    nearbyStatus = NEARBY_ERROR;
    console.log('error when sending request -- status: ' + result.status);

    if (error) {
      error.call(this);
    }
  });
}

/**
 * queries the url given in parameters and calls the appropriate callback
 */
function httpQuery(url, success, error) {
  var request = new XMLHttpRequest();
  
  request.onreadystatechange = function() {
    if (request.status === 200 || request.status === 0) {
      if (request.readyState === 4) {
        success.call(this, request);
      }
    } else {
      sendMessage({NETWORK_ERROR: ""});
      error.call(this, request);
    }
  };
  
  request.open("GET", url, true);
  request.setRequestHeader("Accept-language", "fr_FR");
  request.send(null);
  
  return null;
}

//////////////////////////////////////////////////////////////////////////
/**
 * listen for PebbleKit ready message
 */
Pebble.addEventListener("ready", function(e) {
  // m'kay
});

/**
 * react to app messages sent from Pebble
 */
Pebble.addEventListener("appmessage", function(e) {
  if (e.payload.hasOwnProperty('GET_LOCATION')) {
    if (locationStatus == LOCATION_FOUND) {
      // timeout avoids next message from pebble to get lost...
      setTimeout(function() {
        sendMessage({LOCATION_STATUS: locationStatus});
      }, 50);
    } else {
      waitingForLocation = true;
      if (locationStatus == LOCATION_ERROR) {
        refreshPosition();
      }
    }
  }
  
  if (e.payload.hasOwnProperty('GET_NEARBY')) {
    cancelNearby = false;
    if (locationStatus === LOCATION_FOUND) {
      getNearbyStops();
    }
  }
  
  if (e.payload.hasOwnProperty('GET_STOPS_WAIT')) {
    getWaitTimeForNearbyStops();
  }
  
  if (e.payload.hasOwnProperty('NEARBY_PROCESSED')) {
    previousNearbyMessageProcessed = true;
  }
  
  if (e.payload.hasOwnProperty('ADD_FAVORITE')) {
    toggleFavorite(e.payload['ADD_FAVORITE']);
  }
  
  if (e.payload.hasOwnProperty('GET_FAVORITES')) {
    cancelFavorites = false;
    sendMessage({FAVORITES_NUM: favorites.length});
    sendFavorites();
  }
  
  if (e.payload.hasOwnProperty('GET_FAVORITES_WAIT')) {
    sendWaitTimeForFavorites();
  }
  
  if (e.payload.hasOwnProperty('CANCEL_NEARBY_FETCH')) {
    cancelNearby = true;
  }
  
  if (e.payload.hasOwnProperty('CANCEL_FAVORITES_FETCH')) {
    cancelFavorites = true;
  }
});
