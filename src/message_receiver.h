#include <pebble.h>

#define LOCATION_COMPUTING 0
#define LOCATION_FOUND 1
#define LOCATION_ERROR 2

void set_data_updated_callback(void (*f)(unsigned));

void set_error_received_callback(void (*f)(unsigned));

void message_receiver_init();

int get_location_status();

int is_refreshing_nearby();
