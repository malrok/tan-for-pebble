#include <pebble.h>
#include "home_window.h"
#include "navigation_state.h"
#include "colors.h"

// BEGIN AUTO-GENERATED UI CODE; DO NOT MODIFY
static Window *s_window;
static MenuLayer *s_menulayer_1;

static void initialise_ui(void) {
  s_window = window_create();
  #ifndef PBL_SDK_3
    window_set_fullscreen(s_window, 0);
  #endif
  
  // s_menulayer_1
  s_menulayer_1 = menu_layer_create(GRect(0, 0, PBL_DISPLAY_WIDTH, PBL_DISPLAY_HEIGHT));
  menu_layer_set_click_config_onto_window(s_menulayer_1, s_window);
  layer_add_child(window_get_root_layer(s_window), (Layer *)s_menulayer_1);
}

static void destroy_ui(void) {
  window_destroy(s_window);
  menu_layer_destroy(s_menulayer_1);
}
// END AUTO-GENERATED UI CODE

static uint16_t menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  return 2;
}

static int16_t menu_get_cell_height_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {
  return MENU_CELL_ROUND_FOCUSED_TALL_CELL_HEIGHT;
}

static void menu_draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data) {
  menu_cell_basic_draw(ctx, cell_layer, cell_index->row == 0 ? "Favoris" : "A proximité", "", NULL);
}

static void menu_select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data) {
  if (cell_index->row == 0) {
    show_next(STATE_FAVORITES);
  } else {
    show_next(STATE_NEARBY);
  }
}

static void handle_window_unload(Window* window) {
  destroy_ui();
}

static void handle_window_appear(Window* window) {
  set_current_state(STATE_HOME);
}

static void prepare_menu_layer() {
  menu_layer_set_callbacks(s_menulayer_1, NULL, (MenuLayerCallbacks){
    .get_num_rows = menu_get_num_rows_callback,
    .draw_row = menu_draw_row_callback,
    .select_click = menu_select_callback,
    .get_cell_height = menu_get_cell_height_callback,
  });
  
  // keeps scroll on screen
  menu_layer_pad_bottom_enable(s_menulayer_1, false);
  menu_layer_set_center_focused(s_menulayer_1, false);
  
  // set colors
  menu_layer_set_highlight_colors(s_menulayer_1, GREEN, GColorFromRGB(255, 255, 255));
}

void show_home_window(void) {
  initialise_ui();
  
  init_navigation_state();
  
  prepare_menu_layer();
  
  window_set_window_handlers(s_window, (WindowHandlers) {
    .unload = handle_window_unload,
    .appear = handle_window_appear,
  });
  window_stack_push(s_window, true);
}

void hide_home_window(void) {
  window_stack_remove(s_window, true);
}

