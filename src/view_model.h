#pragma once

typedef struct s_wait {
  
  int way;
  int favorite;
  char *terminus;
  char *wait;
  
} wait;

typedef struct s_line {
  
  int favorite_line;
  char *num;
  
  int waits_number;
  
  struct s_wait *waits;
  
} line;

typedef struct s_viewmodel {
  
  char *stop_code;
  char *stop_name;
  char *stop_distance;
  char pagination_text[8];
  
  int lines_number;
  
  struct s_line *lines;
  
} viewmodel;

extern int build_nearby_view_model(int rank);
extern void build_stop_view_model(char *);
extern void build_favorites_view_model();

extern viewmodel *get_view_model();
extern viewmodel *get_list_view_model();

extern int get_list_size();
extern int list_remove_item(int);

