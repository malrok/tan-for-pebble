#include <pebble.h>
#include "loading_window.h"
#include "colors.h"

// BEGIN AUTO-GENERATED UI CODE; DO NOT MODIFY
static Window *s_window;
static GBitmap *s_res_image_loading_identifier;
static BitmapLayer *loading_bitmaplayer;

static void initialise_ui(void) {
  s_window = window_create();
  #ifndef PBL_SDK_3
    window_set_fullscreen(s_window, true);
  #endif
  
  s_res_image_loading_identifier = gbitmap_create_with_resource(RESOURCE_ID_image_loading_identifier);
  // loading_bitmaplayer
  loading_bitmaplayer = bitmap_layer_create(GRect(32, 44, 80, 80));
  bitmap_layer_set_bitmap(loading_bitmaplayer, s_res_image_loading_identifier);
  bitmap_layer_set_background_color(loading_bitmaplayer, GColorWhite);
  layer_add_child(window_get_root_layer(s_window), (Layer *)loading_bitmaplayer);
}

static void destroy_ui(void) {
  window_destroy(s_window);
  bitmap_layer_destroy(loading_bitmaplayer);
  gbitmap_destroy(s_res_image_loading_identifier);
}
// END AUTO-GENERATED UI CODE

static void handle_window_unload(Window* window) {
  destroy_ui();
}

void show_loading_window(void) {
  initialise_ui();
  
  bitmap_layer_set_compositing_mode(loading_bitmaplayer, GCompOpSet);
  
  window_set_window_handlers(s_window, (WindowHandlers) {
    .unload = handle_window_unload,
  });
  window_stack_push(s_window, true);
}

void hide_loading_window(void) {
  window_stack_remove(s_window, true);
}

