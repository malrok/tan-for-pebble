#include <pebble.h>
#include "delete_favorite_window.h"
#include "navigation_state.h"
#include "delete_favorite_delegate.h"
#include "colors.h"

// BEGIN AUTO-GENERATED UI CODE; DO NOT MODIFY
static Window *s_window;
static GFont s_res_roboto_condensed_21;
static GBitmap *s_res_image_validate_identifier;
static GBitmap *s_res_image_cancel_identifier;
static TextLayer *s_textlayer_1;
static ActionBarLayer *s_actionbarlayer_1;

static void initialise_ui(void) {
  s_window = window_create();
  #ifndef PBL_SDK_3
    window_set_fullscreen(s_window, 0);
  #endif
  
  s_res_roboto_condensed_21 = fonts_get_system_font(FONT_KEY_ROBOTO_CONDENSED_21);
  s_res_image_validate_identifier = gbitmap_create_with_resource(RESOURCE_ID_image_validate_identifier);
  s_res_image_cancel_identifier = gbitmap_create_with_resource(RESOURCE_ID_image_cancel_identifier);
  // s_textlayer_1
  s_textlayer_1 = text_layer_create(GRect(12, 72, 100, 24));
  text_layer_set_text(s_textlayer_1, "Supprimer ?");
  text_layer_set_text_alignment(s_textlayer_1, GTextAlignmentCenter);
  text_layer_set_font(s_textlayer_1, s_res_roboto_condensed_21);
  layer_add_child(window_get_root_layer(s_window), (Layer *)s_textlayer_1);
  
  // s_actionbarlayer_1
  s_actionbarlayer_1 = action_bar_layer_create();
  action_bar_layer_add_to_window(s_actionbarlayer_1, s_window);
  action_bar_layer_set_background_color(s_actionbarlayer_1, GColorWhite);
  action_bar_layer_set_icon(s_actionbarlayer_1, BUTTON_ID_UP, s_res_image_validate_identifier);
  action_bar_layer_set_icon(s_actionbarlayer_1, BUTTON_ID_DOWN, s_res_image_cancel_identifier);
  layer_add_child(window_get_root_layer(s_window), (Layer *)s_actionbarlayer_1);
}

static void destroy_ui(void) {
  window_destroy(s_window);
  text_layer_destroy(s_textlayer_1);
  action_bar_layer_destroy(s_actionbarlayer_1);
  gbitmap_destroy(s_res_image_validate_identifier);
  gbitmap_destroy(s_res_image_cancel_identifier);
}
// END AUTO-GENERATED UI CODE

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
  set_delete(DELETE);
  show_next(STATE_FAVORITES);
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
  set_delete(DONT_DELETE);
  show_next(STATE_FAVORITES);
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

static void handle_window_unload(Window* window) {
  destroy_ui();
}

void show_delete_favorite_window(void) {
  initialise_ui();
  
  action_bar_layer_set_background_color(s_actionbarlayer_1, GREEN);
  
  // Use this provider to add button click subscriptions
  window_set_click_config_provider(s_window, click_config_provider);
  
  window_set_window_handlers(s_window, (WindowHandlers) {
    .unload = handle_window_unload,
  });
  
  window_stack_push(s_window, true);
}

void hide_delete_favorite_window(void) {
  window_stack_remove(s_window, true);
}

