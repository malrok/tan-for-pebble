#include <pebble.h>

void inbox_received_callback(DictionaryIterator *, void *);

void inbox_dropped_callback(AppMessageResult, void *);

void outbox_failed_callback(DictionaryIterator *, AppMessageResult, void *);

void outbox_sent_callback(DictionaryIterator *, void *);

void send_int(int, int);

void send_string(int, char *);

void send_message(int);

void set_received_callback(void (*f)(DictionaryIterator*));

void init_comm_utils();
