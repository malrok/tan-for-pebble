#include <pebble.h>
#include "delete_favorite_delegate.h"

static int result;

void set_delete(int delete) {
  result = delete;
}

int get_delete() {
  return result;
}
