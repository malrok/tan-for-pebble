#pragma once

#define STATE_HOME 0
#define STATE_FAVORITES 1
#define STATE_DELETE 2
#define STATE_NEARBY 3
#define STATE_STOP 4
#define STATE_LOADING_FAVORITES 5
#define STATE_LOADING_NEARBY 6
#define STATE_NEARBY_ERROR 7
#define STATE_FAVORITES_ERROR 8
#define STATE_FAVORITES_BUFFERED 9
#define STATE_NEARBY_BUFFERED 10
#define STATE_NETWORK_ERROR 11

void init_navigation_state();

void set_current_state(int);

int get_current_state();

void show_next_with_message(int, char*);

void show_next(int);

void destroy_home();
