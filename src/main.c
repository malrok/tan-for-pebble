#include <pebble.h>
#include "comm_utils.h"
#include "message_receiver.h"
#include "navigation_state.h"

int loading = 1;

static void init() {
  // init communication layer
  init_comm_utils();
  
  // init message receiver class
  message_receiver_init();
  
  //show_loading_window();
  show_next(STATE_HOME);
}

static void deinit() {
  destroy_home();
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}
