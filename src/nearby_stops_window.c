#include <pebble.h>
#include "nearby_stops_window.h"
#include "constants.h"
#include "message_receiver.h"
#include "view_model.h"
#include "navigation_state.h"
#include "colors.h"
#include "comm_utils.h"

static int card_rank = 0;
static int computing_vm = 0;
static int just_appeared = 0;

// BEGIN AUTO-GENERATED UI CODE; DO NOT MODIFY
static Window *s_window;
static GFont s_res_gothic_14;
static GFont s_res_gothic_18_bold;
static GFont s_res_gothic_24_bold;
static GFont s_res_gothic_18;
static Layer *titlebackground_layer;
static TextLayer *line1direction1_textlayer;
static TextLayer *line1time1_textlayer;
static TextLayer *line1_textlayer;
static TextLayer *line1direction2_textlayer;
static TextLayer *line1time2_textlayer;
static TextLayer *stop_textlayer;
static TextLayer *line2_textlayer;
static TextLayer *line2direction1_textlayer;
static TextLayer *line2direction2_textlayer;
static TextLayer *line2time1_textlayer;
static TextLayer *line2time2_textlayer;
static TextLayer *distance_textlayer;
static TextLayer *pagination_textlayer;

static void initialise_ui(void) {
  s_window = window_create();
  #ifndef PBL_SDK_3
    window_set_fullscreen(s_window, true);
  #endif
  
  s_res_gothic_14 = fonts_get_system_font(FONT_KEY_GOTHIC_14);
  s_res_gothic_18_bold = fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD);
  s_res_gothic_24_bold = fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD);
  s_res_gothic_18 = fonts_get_system_font(FONT_KEY_GOTHIC_18);
  // titlebackground_layer
  titlebackground_layer = layer_create(GRect(0, 0, 144, 34));
  layer_add_child(window_get_root_layer(s_window), (Layer *)titlebackground_layer);
  
  // line1direction1_textlayer
  line1direction1_textlayer = text_layer_create(GRect(2, 60, 78, 20));
  text_layer_set_text(line1direction1_textlayer, "Text layer");
  text_layer_set_font(line1direction1_textlayer, s_res_gothic_14);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line1direction1_textlayer);
  
  // line1time1_textlayer
  line1time1_textlayer = text_layer_create(GRect(80, 57, 64, 20));
  text_layer_set_text(line1time1_textlayer, "Text layer");
  text_layer_set_text_alignment(line1time1_textlayer, GTextAlignmentRight);
  text_layer_set_font(line1time1_textlayer, s_res_gothic_18_bold);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line1time1_textlayer);
  
  // line1_textlayer
  line1_textlayer = text_layer_create(GRect(2, 35, 30, 24));
  text_layer_set_text(line1_textlayer, "C1");
  text_layer_set_font(line1_textlayer, s_res_gothic_24_bold);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line1_textlayer);
  
  // line1direction2_textlayer
  line1direction2_textlayer = text_layer_create(GRect(2, 81, 78, 20));
  text_layer_set_text(line1direction2_textlayer, "Layer de texte");
  text_layer_set_font(line1direction2_textlayer, s_res_gothic_14);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line1direction2_textlayer);
  
  // line1time2_textlayer
  line1time2_textlayer = text_layer_create(GRect(80, 77, 64, 20));
  text_layer_set_text(line1time2_textlayer, "Layer de texte");
  text_layer_set_text_alignment(line1time2_textlayer, GTextAlignmentRight);
  text_layer_set_font(line1time2_textlayer, s_res_gothic_18_bold);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line1time2_textlayer);
  
  // stop_textlayer
  stop_textlayer = text_layer_create(GRect(0, 12, 144, 20));
  text_layer_set_background_color(stop_textlayer, GColorClear);
  text_layer_set_text_color(stop_textlayer, GColorWhite);
  text_layer_set_text(stop_textlayer, "Layer de texte");
  text_layer_set_text_alignment(stop_textlayer, GTextAlignmentCenter);
  text_layer_set_font(stop_textlayer, s_res_gothic_18);
  layer_add_child(window_get_root_layer(s_window), (Layer *)stop_textlayer);
  
  // line2_textlayer
  line2_textlayer = text_layer_create(GRect(2, 104, 30, 24));
  text_layer_set_text(line2_textlayer, "C10");
  text_layer_set_font(line2_textlayer, s_res_gothic_24_bold);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line2_textlayer);
  
  // line2direction1_textlayer
  line2direction1_textlayer = text_layer_create(GRect(2, 129, 78, 20));
  text_layer_set_text(line2direction1_textlayer, "Layer de texte");
  text_layer_set_font(line2direction1_textlayer, s_res_gothic_14);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line2direction1_textlayer);
  
  // line2direction2_textlayer
  line2direction2_textlayer = text_layer_create(GRect(2, 149, 78, 20));
  text_layer_set_text(line2direction2_textlayer, "Layer de texte");
  text_layer_set_font(line2direction2_textlayer, s_res_gothic_14);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line2direction2_textlayer);
  
  // line2time1_textlayer
  line2time1_textlayer = text_layer_create(GRect(80, 125, 64, 20));
  text_layer_set_text(line2time1_textlayer, "Layer de texte");
  text_layer_set_text_alignment(line2time1_textlayer, GTextAlignmentRight);
  text_layer_set_font(line2time1_textlayer, s_res_gothic_18_bold);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line2time1_textlayer);
  
  // line2time2_textlayer
  line2time2_textlayer = text_layer_create(GRect(80, 145, 64, 20));
  text_layer_set_text(line2time2_textlayer, "Layer de texte");
  text_layer_set_text_alignment(line2time2_textlayer, GTextAlignmentRight);
  text_layer_set_font(line2time2_textlayer, s_res_gothic_18_bold);
  layer_add_child(window_get_root_layer(s_window), (Layer *)line2time2_textlayer);
  
  // distance_textlayer
  distance_textlayer = text_layer_create(GRect(2, 0, 42, 14));
  text_layer_set_background_color(distance_textlayer, GColorClear);
  text_layer_set_text_color(distance_textlayer, GColorWhite);
  text_layer_set_text(distance_textlayer, "17\"");
  text_layer_set_font(distance_textlayer, s_res_gothic_14);
  layer_add_child(window_get_root_layer(s_window), (Layer *)distance_textlayer);
  
  // pagination_textlayer
  pagination_textlayer = text_layer_create(GRect(76, 0, 66, 14));
  text_layer_set_background_color(pagination_textlayer, GColorClear);
  text_layer_set_text_color(pagination_textlayer, GColorWhite);
  text_layer_set_text(pagination_textlayer, "Text layer");
  text_layer_set_text_alignment(pagination_textlayer, GTextAlignmentRight);
  text_layer_set_font(pagination_textlayer, s_res_gothic_14);
  layer_add_child(window_get_root_layer(s_window), (Layer *)pagination_textlayer);
}

static void destroy_ui(void) {
  window_destroy(s_window);
  layer_destroy(titlebackground_layer);
  text_layer_destroy(line1direction1_textlayer);
  text_layer_destroy(line1time1_textlayer);
  text_layer_destroy(line1_textlayer);
  text_layer_destroy(line1direction2_textlayer);
  text_layer_destroy(line1time2_textlayer);
  text_layer_destroy(stop_textlayer);
  text_layer_destroy(line2_textlayer);
  text_layer_destroy(line2direction1_textlayer);
  text_layer_destroy(line2direction2_textlayer);
  text_layer_destroy(line2time1_textlayer);
  text_layer_destroy(line2time2_textlayer);
  text_layer_destroy(distance_textlayer);
  text_layer_destroy(pagination_textlayer);
}
// END AUTO-GENERATED UI CODE

static void empty_fields() {
  text_layer_set_text(line1direction1_textlayer, "");
  text_layer_set_text(line1time1_textlayer, "");
  text_layer_set_text(line1_textlayer, "");
  text_layer_set_text(line1direction2_textlayer, "");
  text_layer_set_text(line1time2_textlayer, "");
  text_layer_set_text(stop_textlayer, "");
  text_layer_set_text(line2_textlayer, "");
  text_layer_set_text(line2direction1_textlayer, "");
  text_layer_set_text(line2direction2_textlayer, "");
  text_layer_set_text(line2time1_textlayer, "");
  text_layer_set_text(line2time2_textlayer, "");
  text_layer_set_text(distance_textlayer, "");
  text_layer_set_text(pagination_textlayer, "");
}

static void draw_window(int direction, int vm_ok) {
  if (card_rank >= 0 && vm_ok != 0) {
    
    empty_fields();
    
    text_layer_set_text(distance_textlayer, get_view_model()->stop_distance);
    text_layer_set_text(stop_textlayer, get_view_model()->stop_name);
    text_layer_set_text(pagination_textlayer, get_view_model()->pagination_text);
    
    if (get_view_model()->lines_number > 0) {
      text_layer_set_text(line1_textlayer, get_view_model()->lines[0].num); //num
      
      if (get_view_model()->lines[0].waits_number > 0) {
        text_layer_set_text(line1direction1_textlayer, get_view_model()->lines[0].waits[0].terminus);
        text_layer_set_text(line1time1_textlayer, get_view_model()->lines[0].waits[0].wait);
      }
      if (get_view_model()->lines[0].waits_number > 1) {
        text_layer_set_text(line1direction2_textlayer, get_view_model()->lines[0].waits[1].terminus);
        text_layer_set_text(line1time2_textlayer, get_view_model()->lines[0].waits[1].wait);
      }
    }
    
    if (get_view_model()->lines_number > 1) {
      text_layer_set_text(line2_textlayer, get_view_model()->lines[1].num); //num
      
      if (get_view_model()->lines[1].waits_number > 0) {
        text_layer_set_text(line2direction1_textlayer, get_view_model()->lines[1].waits[0].terminus);
        text_layer_set_text(line2time1_textlayer, get_view_model()->lines[1].waits[0].wait);
      }
      if (get_view_model()->lines[1].waits_number > 1) {
        text_layer_set_text(line2direction2_textlayer, get_view_model()->lines[1].waits[1].terminus);
        text_layer_set_text(line2time2_textlayer, get_view_model()->lines[1].waits[1].wait);
      }
    }
    
  } else {
    // TODO animate bounce
    if (direction == UP) {
      card_rank++;
    } else {
      card_rank--;
    }
  }
}

static void refresh_nearby_stops(int direction) {
  int vm_ok = build_nearby_view_model(card_rank);
  draw_window(direction, vm_ok);
  computing_vm = 0;
}

static void message_received(unsigned message) {
  if (message == MESSAGE_KEY_WAIT_FOR_STOP) {
    refresh_nearby_stops(NONE);
  }
}

static void tick_callback(struct tm *tick_time, TimeUnits units_changed) {
  if (just_appeared == 1) {
    just_appeared = 0;
  } else {
    send_message(MESSAGE_KEY_GET_NEARBY);
  }
}

static void update_titlebackground_layer(Layer *layer, GContext *ctx) {
  graphics_context_set_fill_color(ctx, GREEN);
  graphics_fill_rect(ctx, layer_get_bounds(titlebackground_layer), 0, GCornerNone);
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
  if (computing_vm == 0) {
    computing_vm = 1;
    card_rank--;
    refresh_nearby_stops(UP);
  }
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
  // A single click has just occured
  show_next_with_message(STATE_STOP, get_view_model()->stop_code);
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
  if (computing_vm == 0) {
    computing_vm = 1;
    card_rank++;
    refresh_nearby_stops(DOWN);
  }
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

static void handle_window_unload(Window* window) {
  destroy_ui();
}

static void handle_window_appear(Window* window) {
  set_current_state(STATE_NEARBY);
  
  set_data_updated_callback(message_received);
  
  refresh_nearby_stops(NONE);
  
  just_appeared = 1;
  
  tick_timer_service_subscribe(MINUTE_UNIT, tick_callback);
}

static void handle_window_disappear(Window* window) {
  tick_timer_service_unsubscribe();
  set_data_updated_callback(NULL);
  send_message(MESSAGE_KEY_CANCEL_NEARBY_FETCH);
}

void show_nearby_stops_window(void) {
  initialise_ui();
  
  layer_set_update_proc(titlebackground_layer, update_titlebackground_layer);
  
  // Use this provider to add button click subscriptions
  window_set_click_config_provider(s_window, click_config_provider);
  
  window_set_window_handlers(s_window, (WindowHandlers) {
    .unload = handle_window_unload,
    .appear = handle_window_appear,
    .disappear = handle_window_disappear,
  });
  window_stack_push(s_window, true);
}

void hide_nearby_stops_window(void) {
  window_stack_remove(s_window, true);
}

