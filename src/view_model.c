#include <pebble.h>
#include "view_model.h"
#include "tan_data.h"
#include "cJSON.h"
#include "constants.h"
#include "small_maths.h"

struct s_viewmodel *list_vm;
viewmodel *vm;
int list_size = 0;

/**
 * frees the viewmodel object
 */
static void delete_vm() {
  free(vm->lines);
  free(vm);
}

/**
 * builds a viewmodel an extracts a number of wait objects
 */
static void build_nearby_vm(cJSON *tan_stop, int start, int end) {
  int line_number = start, index = 0, waits_size;
  
  if (vm != NULL) {
    delete_vm();
  }

  vm = malloc(sizeof(viewmodel));

  vm->stop_code     = cJSON_GetObjectItem(tan_stop, CODELIEU)->valuestring;
  vm->stop_name     = cJSON_GetObjectItem(tan_stop, LIBELLE)->valuestring;
  vm->stop_distance = cJSON_GetObjectItem(tan_stop, DISTANCE)->valuestring;
  vm->lines_number  = end - start;

  vm->lines = malloc((end - start) * sizeof(line));

  for (; line_number < end; line_number++) {
    cJSON *tan_line = cJSON_GetArrayItem(cJSON_GetObjectItem(tan_stop, LIGNE), line_number);

    line* new_line = malloc(sizeof(line));

    new_line->num           = cJSON_GetObjectItem(tan_line, NUMLIGNE)->valuestring;
    new_line->favorite_line = cJSON_GetObjectItem(tan_line, LIGNEFAVORITE)->valueint;
    
    waits_size = cJSON_GetArraySize(cJSON_GetObjectItem(tan_line, WAIT));
    
    new_line->waits         = malloc(waits_size * sizeof(wait));
    new_line->waits_number  = waits_size;
    
    for (int wait_number = 0; wait_number < waits_size; wait_number++) {
      cJSON *tan_wait = cJSON_GetArrayItem(cJSON_GetObjectItem(tan_line, WAIT), wait_number);
      
      wait* new_wait = malloc(sizeof(wait));
      
      new_wait->terminus = cJSON_GetObjectItem(tan_wait, TERMINUS)->valuestring;
      new_wait->wait     = cJSON_GetObjectItem(tan_wait, TEMPS)->valuestring;
      new_wait->way      = cJSON_GetObjectItem(tan_wait, SENS)->valueint;
      new_wait->favorite = cJSON_GetObjectItem(tan_wait, FAVORIS)->valueint;
      
      new_line->waits[wait_number] = *new_wait;
    }
    vm->lines[index++] = *new_line;
    
    free(new_line);
  }
}

/**
 * builds a viewmodel for the nearby_stops window
 */
int build_nearby_view_model(int rank) {
  int ret = 0, start, end;
  int reached = 0;
  
  cJSON *tan_stop, *data = get_tan_data();
  
  for (int i = 0 ; i < get_tan_data_size() ; i++) {
    
    tan_stop = cJSON_GetArrayItem(data, i);
    
    int lines_for_stop = cJSON_GetObjectItem(tan_stop, LIGNESIZE)->valueint;
    int screens_for_stop = lines_for_stop / LINES_PER_SCREEN;

    if (lines_for_stop % LINES_PER_SCREEN > 0) {
      screens_for_stop++;
    }
    
    if (reached == rank) {
      end = sm_min(cJSON_GetObjectItem(tan_stop, LIGNESIZE)->valueint, LINES_PER_SCREEN);
      
      build_nearby_vm(tan_stop, 0 ,end);
      
      ret = 1;
    } else if (reached < rank) {
      if (screens_for_stop + reached - 1 >= rank) {
        start = (rank - reached) * LINES_PER_SCREEN;
        end = sm_min(lines_for_stop, start + LINES_PER_SCREEN);
        
        build_nearby_vm(tan_stop, start, end);
        
        ret = 1;
      }
    }
    reached += screens_for_stop;
  }
  
  snprintf(vm->pagination_text, sizeof(vm->pagination_text), "%i/%i", rank + 1, reached);
  
  return ret;
}

/**
 * builds a viewmodel an extracts a number of wait objects
 */
static viewmodel* build_stop_vm(cJSON *tan_stop) {
  viewmodel *ret;
  
  int end = cJSON_GetObjectItem(tan_stop, LIGNESIZE)->valueint;
  int line_number = 0, index = 0, waits_size;
  
  ret = malloc(sizeof(viewmodel));
  
  ret->stop_code     = cJSON_GetObjectItem(tan_stop, CODELIEU)->valuestring;
  ret->stop_name     = cJSON_GetObjectItem(tan_stop, LIBELLE)->valuestring;
  ret->stop_distance = cJSON_GetObjectItem(tan_stop, DISTANCE)->valuestring;
  
  ret->lines = malloc(sizeof(line));
  
  for (; line_number < end; line_number++) {
    cJSON *tan_line = cJSON_GetArrayItem(cJSON_GetObjectItem(tan_stop, LIGNE), line_number);
    
    waits_size = cJSON_GetArraySize(cJSON_GetObjectItem(tan_line, WAIT));
    
    for (int wait_number = 0; wait_number < waits_size; wait_number++) {
      line* new_line = malloc(sizeof(line));
      
      new_line->num           = cJSON_GetObjectItem(tan_line, NUMLIGNE)->valuestring;
      new_line->favorite_line = cJSON_GetObjectItem(tan_line, LIGNEFAVORITE)->valueint;
      
      new_line->waits         = malloc(sizeof(wait));
      new_line->waits_number  = 1;
      
      cJSON *tan_wait = cJSON_GetArrayItem(cJSON_GetObjectItem(tan_line, WAIT), wait_number);
      
      wait* new_wait = malloc(sizeof(wait));
      
      new_wait->terminus = cJSON_GetObjectItem(tan_wait, TERMINUS)->valuestring;
      new_wait->wait     = cJSON_GetObjectItem(tan_wait, TEMPS)->valuestring;
      new_wait->way      = cJSON_GetObjectItem(tan_wait, SENS)->valueint;
      new_wait->favorite = cJSON_GetObjectItem(tan_wait, FAVORIS)->valueint;
      
      new_line->waits[0] = *new_wait;
      
      if (index > 0) {
        ret->lines = realloc(vm->lines, (index + 1) * sizeof(line));
      }
      
      ret->lines[index] = *new_line;
      
      index++;
    }
  }
  
  ret->lines_number = index;
  
  return ret;
}

/**
 * builds a viewmodel for the stop_waits_window
 */
extern void build_stop_view_model(char *stop_code) {
  cJSON *tan_stop, *data = get_tan_data();
  
  if (vm != NULL) {
    delete_vm();
  }
  
  for (int i = 0 ; i < get_tan_data_size() ; i++) {
    tan_stop = cJSON_GetArrayItem(data, i);
    if (strcmp(cJSON_GetObjectItem(tan_stop, CODELIEU)->valuestring, stop_code) == 0) {
      vm = build_stop_vm(tan_stop);
      break;
    }
  }
}

void build_favorites_view_model() {
  if (list_vm != NULL) {
    free(list_vm);
  }
  
  cJSON *tan_stop, *data = get_tan_data();
  list_size = get_tan_data_size();
  
  list_vm = malloc(list_size * sizeof(viewmodel));
  
  for (int i = 0 ; i < list_size ; i++) {
    tan_stop = cJSON_GetArrayItem(data, i);
    
    list_vm[i] = *build_stop_vm(tan_stop);
  }
}

/**
 * returns the viewmodel object
 */
viewmodel *get_view_model() {
  return vm;
}

viewmodel *get_list_view_model() {
  return list_vm;
}

int get_list_size() {
  return list_size;
}

int list_remove_item(int index) {
  for (int i = index; i < list_size - 1; i++) {
    list_vm[i] = list_vm[i + 1];
  }
  
  list_size--;
  
  if (list_size == 0) {
    clear_data_type(FAVORITES_TYPE);
  }
  
  list_vm = realloc(list_vm, list_size * sizeof(viewmodel));
  
  return list_size;
}
