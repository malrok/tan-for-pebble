#include <pebble.h>

/** conf */
#define LINES_PER_SCREEN 2

/** json minification */
#define CODELIEU "c"
#define LIBELLE "l"
#define DISTANCE "d"
#define LIGNE "g"
#define NUMLIGNE "n"
#define SENS "s"
#define TERMINUS "t"
#define INFOTRAFIC "i"
#define TEMPS "p"
#define TYPELIGNE "y"
#define ARRET "a"
#define CODEARRET "r"
#define FAVORIS "f"
#define LIGNEFAVORITE "e"
#define WAIT "w"
#define LIGNESIZE "z"

