#include <pebble.h>
#include "constants.h"
#include "tan_data.h"
#include "cJSON.h"

int current_type = VOID_TYPE,
    has_nearby_data = 0,
    has_favorites_data = 0;
int favorites_root_data_size = 0,
    nearby_root_data_size = 0;
cJSON *favorites_root_data,
      *nearby_root_data,
      *temp_data;

void init_tan_data(int type) {
  current_type = type;
  
  if (type == FAVORITES_TYPE) {
    has_favorites_data = 1;
    favorites_root_data_size = 0;
  } else {
    has_nearby_data = 1;
    nearby_root_data_size = 0;
  }
  
  temp_data = cJSON_CreateArray();
}

void add_tan_data(char *stream) {
  // parse the json answer
  cJSON *child = cJSON_Parse(stream);
  
  if (current_type == FAVORITES_TYPE) {
    favorites_root_data_size++;
  } else {
    nearby_root_data_size++;
  }
  
  cJSON_AddItemToArray(temp_data, child);
}

void extend_tan_data(char *stream) {
  cJSON *leaf_data = cJSON_Parse(stream);
  cJSON *child;
  
  int size = nearby_root_data_size;
  
  if (current_type == FAVORITES_TYPE) {
    size = favorites_root_data_size;
  }
  
  for (int i = 0 ; i < size ; i++) {
    child = cJSON_GetArrayItem(temp_data, i);
    if (strcmp(cJSON_GetObjectItem(child, CODELIEU)->valuestring, cJSON_GetObjectItem(leaf_data, CODELIEU)->valuestring) == 0) {
      cJSON_AddItemToObject(child, LIGNE, cJSON_GetObjectItem(leaf_data, LIGNE));
      cJSON_AddNumberToObject(child, LIGNESIZE, cJSON_GetArraySize(cJSON_GetObjectItem(leaf_data, LIGNE)));
      break;
    }
  }
}

void push_tan_data() {
  if (current_type == FAVORITES_TYPE) {
    if (favorites_root_data != NULL) {
      cJSON_Delete(favorites_root_data);
    }
    favorites_root_data = temp_data;
  } else {
    if (nearby_root_data != NULL) {
      cJSON_Delete(nearby_root_data);
    }
    nearby_root_data = temp_data;
  }
}

int get_tan_data_size() {
  if (current_type == FAVORITES_TYPE) {
    return favorites_root_data_size;
  } else {
    return nearby_root_data_size;
  }
}

cJSON *get_tan_data() {
  if (current_type == FAVORITES_TYPE) {
    return favorites_root_data;
  } else {
    return nearby_root_data;
  }
}

int has_data_type(int type) {
  if (type == FAVORITES_TYPE) {
    return has_favorites_data == 1;
  } else {
    return has_nearby_data == 1;
  }
}

void set_data_type(int type) {
  current_type = type;
}

void clear_data_type(int type) {
  if (type == FAVORITES_TYPE) {
    has_favorites_data = 0;
    favorites_root_data_size = 0;
  } else {
    has_nearby_data = 0;
    nearby_root_data_size = 0;
  }
}
